<?php 
/*----------------------------------------------------------------*\

	TEAMMEMBER SINGLE POST TEMPLATE

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php get_template_part('template-parts/headers/header-teammember'); ?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>

			<section class="get-to-know is-standard-width has-standard-spacing">

				<?php if ( get_field('baby_photo') ) : ?>
					<?php $babyimage = get_field('baby_photo'); ?>
					<div>
						<img src="<?php echo $babyimage['sizes']['small']; ?>" alt="<?php echo $babyimage['alt']; ?>" />
					</div>
				<?php else : ?>
					<div>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-bio-image.jpg" alt="default headshot" />
					</div>
				<?php endif; ?>

				<?php if ( get_field('get_to_know') ) : ?>
					<?php
						$s = get_the_title();
						$arr1 = explode(' ',trim($s));
					?>
					<div>
						<h2>Get to know <?php echo $arr1[0]."\n"; ?></h2>
						<?php the_field('get_to_know'); ?>
					</div>
				<?php endif; ?>

			</section>

			<section class="button-section is-standard-width text-center">
				<?php
						$link = '/registered-investment-advisors/'
					?>
				<a class="button is-primary is-massive" href="<?php echo get_site_url(); ?><?php echo $link; ?>">See the Whole Team</a>
			</section>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>