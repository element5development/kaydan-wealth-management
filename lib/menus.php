<?php

/*----------------------------------------------------------------*\

	CUSTOM MENU AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_nav' => __( 'Primary Navigation' ),
		'footer_nav' => __( 'Footer Navigation' ),
		'legal_nav' => __( 'Legal Navigation' ),
		'event_type_nav' => __( 'Event Type Navigation' ),
		'category_nav' => __( 'Category Navigation' )
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );

/*----------------------------------------------------------------*\
	ENABLE YOAST BREADCRUMBS
\*----------------------------------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );
function jb_crumble_bread($link_text, $id) {
	$link_text = html_entity_decode($link_text);
	$crumb_length = strlen( $link_text );
 	$crumb_size = 14;
 	$crumble = substr( $link_text, 0, $crumb_size );
	if ( $crumb_length > $crumb_size ) {
		$crumble .= '...';
	}
	return $crumble;
}
add_filter('wp_seo_get_bc_title', 'jb_crumble_bread', 10, 2);