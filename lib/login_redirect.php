<?php

/*----------------------------------------------------------------*\
	LOGIN REDIRECT FOR USERS OTHER THAN ADMINS
\*----------------------------------------------------------------*/
function my_login_redirect( $url, $request, $user ){
	if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
	if( $user->has_cap( 'administrator')) {
	$url = admin_url();
	} else {
	$url = home_url('/employees/');
	}
	}
	return $url;
}
add_filter('login_redirect', 'my_login_redirect', 10, 3 );