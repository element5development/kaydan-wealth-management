<?php

/*----------------------------------------------------------------*\
	CUSTOM CSS FOR WP ADMIN AREA
\*----------------------------------------------------------------*/
function my_custom_admin() {
  echo '
    <style>
      #wpcontent { max-width:100% !important; min-height: calc(100vh - 110px); }
      .flatty-top-bar-button, .flatty-top-bar a:hover, .flatty-top-bar-button, 
      .flatty-top-bar a:active { 
        color: #fff; 
      }
      #adminmenu, #adminmenu .wp-submenu, #adminmenuback, #adminmenuwrap { 
        background-color: #23343f; 
      }
      #adminmenu .wp-submenu-head, #adminmenu a.menu-top:hover,
      #adminmenu .wp-submenu-head, #adminmenu a.menu-top:focus,
      #adminmenu .wp-submenu-head, #adminmenu a.menu-top * { color: #fff; }
      #adminmenu div.wp-menu-image:before { color: #fff !important; }
      #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, 
      #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, 
      #adminmenu li.current a.menu-top, 
      #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, 
      .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu {
        background-color: #e48849;
      }
      #adminmenu .wp-has-current-submenu .wp-submenu, 
      #adminmenu .wp-has-current-submenu .wp-submenu.sub-open, 
      #adminmenu .wp-has-current-submenu.opensub .wp-submenu, 
      #adminmenu a.wp-has-current-submenu:focus + .wp-submenu, 
      .no-js li.wp-has-current-submenu:hover .wp-submenu, 
      #adminmenu li.menu-top:hover, 
      #adminmenu li.opensub > a.menu-top, #adminmenu li > a.menu-top:focus, 
      #wpadminbar .ab-top-menu > li.hover > .ab-item, 
      #wpadminbar.nojq .quicklinks .ab-top-menu > li > .ab-item:focus, #wpadminbar:not(.mobile) .ab-top-menu > li:hover > .ab-item, 
      #wpadminbar:not(.mobile) .ab-top-menu > li > .ab-item:focus {
        background: #5b8da1;
      }
      #adminmenu .wp-submenu a { color: #fff; }
      #adminmenu .wp-submenu a:hover, #adminmenu .wp-submenu a:focus { background-color: #23343f; color: #fff; }
      a:hover, a:active { color: #e48849; }
      .wp-core-ui .button-primary, .wrap .add-new-h2, .wrap .add-new-h2:active, 
      .wrap .page-title-action, .wrap .page-title-action:active {     
        background: #e48849 !important;
        border-color: #e48849 !important;
        text-shadow: none !important;
        box-shadow: none !important;
        color: #fff !important;
      }
      .wp-core-ui .button-primary:hover, .wrap .add-new-h2:hover,
      .wrap .page-title-action:hover {
        background-color: #ce7b42 !important;
        border-color: #ce7b42 !important;
        color: #fff !important;
      }
      .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover,
      .wp-core-ui .button:focus, .wp-core-ui .button:hover {
        box-shadow: none;
      }
      .business_card_widget_content { background: #23343f; color: #5a8ba0; }
      .business_card_widget_content .business_card_widget_inner { 
        display: flex; 
        flex-wrap: nowrap; 
      }
      .business_card_widget_content .business_card_widget_data {     
        text-align: right;
        line-height: 1; 
        align-items: flex-end;
      }
      .business_card_widget_content .card-company { height: auto; }
      .business_card_widget_content .card-picture {     
        box-shadow: none !important;
        border-radius: 0;
        width: auto;
        height: 75px; 
      }
    </style>
  ';
}
add_action('admin_head', 'my_custom_admin');