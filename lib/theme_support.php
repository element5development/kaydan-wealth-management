<?php

/*----------------------------------------------------------------*\
	EXTERNAL JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
	HTML 5 SUPPORT
\*----------------------------------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*----------------------------------------------------------------*\
	ENABLE FEATURED IMAGES
\*----------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*----------------------------------------------------------------*\
	ADD & CUSTOMIZE EXCERPTS
\*----------------------------------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
function get_excerpt($limit, $source = null){
	$excerpt = get_the_excerpt();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'...';
	return $excerpt;
}

/*----------------------------------------------------------------*\
	APPLY THEME CSS TO EDITOR
\*----------------------------------------------------------------*/
add_editor_style('/assets/styles/main.css');

/*----------------------------------------------------------------*\
	REMOVE H1 OPTION FROM EDITOR
\*----------------------------------------------------------------*/
function remove_h1_from_heading($args) {
	$args['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Pre=pre';
	return $args;
}
add_filter('tiny_mce_before_init', 'remove_h1_from_heading' );

/*----------------------------------------------------------------*\
	WOOCOMMERCE INIT
\*----------------------------------------------------------------*/
add_action( 'after_setup_theme', function() {
	add_theme_support( 'woocommerce' );
} );

/*----------------------------------------------------------------*\
	SKIP CART
\*----------------------------------------------------------------*/
add_filter('woocommerce_add_to_cart_redirect', 'themeprefix_add_to_cart_redirect');
function themeprefix_add_to_cart_redirect() {
 global $woocommerce;
 $checkout_url = wc_get_checkout_url();
 return $checkout_url;
}
/*----------------------------------------------------------------*\
	REMOVE WOOCOMMERCE CSS ON PRODUCTS
\*----------------------------------------------------------------*/
add_action( 'template_redirect', 'remove_woocommerce_styles_scripts', 999 );
function remove_woocommerce_styles_scripts() {
	if ( function_exists( 'is_woocommerce' ) ) {
		remove_action('wp_enqueue_scripts', [WC_Frontend_Scripts::class, 'load_scripts']);
		remove_action('wp_print_scripts', [WC_Frontend_Scripts::class, 'localize_printed_scripts'], 5);
		remove_action('wp_print_footer_scripts', [WC_Frontend_Scripts::class, 'localize_printed_scripts'], 5);
	}
}
/*----------------------------------------------------------------*\
	SHOW ALL EVENTS IN EVENT ARCHIVE
\*----------------------------------------------------------------*/
function set_posts_per_page_for_event_cpt( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'event' ) ) {
    $query->set( 'posts_per_page', '-1' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_event_cpt' );