<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Custom Post Type Team Member
// Post Type Key: teammember
function create_teammember_cpt() {

	$labels = array(
		'name' => __( 'Team', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Team Member', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Team', 'textdomain' ),
		'name_admin_bar' => __( 'Team Member', 'textdomain' ),
		'archives' => __( 'Team Member Archives', 'textdomain' ),
		'attributes' => __( 'Team Member Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Team Member:', 'textdomain' ),
		'all_items' => __( 'All Team', 'textdomain' ),
		'add_new_item' => __( 'Add New Team Member', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Team Member', 'textdomain' ),
		'edit_item' => __( 'Edit Team Member', 'textdomain' ),
		'update_item' => __( 'Update Team Member', 'textdomain' ),
		'view_item' => __( 'View Team Member', 'textdomain' ),
		'view_items' => __( 'View Team', 'textdomain' ),
		'search_items' => __( 'Search Team Member', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Team Member', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Team Member', 'textdomain' ),
		'items_list' => __( 'Team list', 'textdomain' ),
		'items_list_navigation' => __( 'Team list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Team list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Team Member', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array('department', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'registered-investment-advisors' ),
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'teammember', $args );

}
add_action( 'init', 'create_teammember_cpt', 0 );

// Register Custom Post Type Event
// Post Type Key: event
function create_event_cpt() {

	$labels = array(
		'name' => __( 'Events', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Event', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Events', 'textdomain' ),
		'name_admin_bar' => __( 'Event', 'textdomain' ),
		'archives' => __( 'Event Archives', 'textdomain' ),
		'attributes' => __( 'Event Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Event:', 'textdomain' ),
		'all_items' => __( 'All Events', 'textdomain' ),
		'add_new_item' => __( 'Add New Event', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Event', 'textdomain' ),
		'edit_item' => __( 'Edit Event', 'textdomain' ),
		'update_item' => __( 'Update Event', 'textdomain' ),
		'view_item' => __( 'View Event', 'textdomain' ),
		'view_items' => __( 'View Events', 'textdomain' ),
		'search_items' => __( 'Search Event', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Event', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event', 'textdomain' ),
		'items_list' => __( 'Events list', 'textdomain' ),
		'items_list_navigation' => __( 'Events list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Events list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Event', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-calendar',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array('type', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'events' ),
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'create_event_cpt', 0 );

// Register Custom Post Type Job
// Post Type Key: job
function create_job_cpt() {

	$labels = array(
		'name' => __( 'Jobs', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Job', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Jobs', 'textdomain' ),
		'name_admin_bar' => __( 'Job', 'textdomain' ),
		'archives' => __( 'Job Archives', 'textdomain' ),
		'attributes' => __( 'Job Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Job:', 'textdomain' ),
		'all_items' => __( 'All Jobs', 'textdomain' ),
		'add_new_item' => __( 'Add New Job', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Job', 'textdomain' ),
		'edit_item' => __( 'Edit Job', 'textdomain' ),
		'update_item' => __( 'Update Job', 'textdomain' ),
		'view_item' => __( 'View Job', 'textdomain' ),
		'view_items' => __( 'View Jobs', 'textdomain' ),
		'search_items' => __( 'Search Job', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Job', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Job', 'textdomain' ),
		'items_list' => __( 'Jobs list', 'textdomain' ),
		'items_list_navigation' => __( 'Jobs list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Jobs list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Job', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-id-alt',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'job', $args );

}
add_action( 'init', 'create_job_cpt', 0 );

function my_pre_get_posts( $query ) {
	// validate
	if( is_admin() ) {
		return $query;
	}
	if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'event' ) {
		$query->set('orderby', 'meta_value');
		$query->set('meta_key', 'event_date');
		$query->set('order', 'ASC');
	}
	// always return
	return $query;
}
add_action('pre_get_posts', 'my_pre_get_posts');