<?php

/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", function () {
	return false;
});

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	CHANGE SUBMIT INPUTS TO BUTTONS
\*----------------------------------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );

/*----------------------------------------------------------------*\
	UPDATE URL INPUTS TO INCLUDE HTTP
\*----------------------------------------------------------------*/
function check_website_field_value( $form ) {
	foreach ( $form['fields'] as &$field ) {  
		if ( 'website' == $field['type'] || ( isset( $field['inputType'] ) && 'website' == $field['inputType']) ) {  
			$value = RGFormsModel::get_field_value($field);  
			if (! empty($value) ) { 
				$field_id = $field['id']; 
				if (! preg_match("~^(?:f|ht)tps?://~i", $value) ) {  
					$value = "http://" . $value;
				}
				$_POST['input_' . $field_id] = $value; 
			}
		}
	}
	return $form;
}
add_filter( 'gform_pre_render', 'check_website_field_value' );
add_filter( 'gform_pre_validation', 'check_website_field_value' );

/*----------------------------------------------------------------*\
	REMOVE ANCHOR FOR BOOK APPOINTMENT FORM
\*----------------------------------------------------------------*/
add_filter( 'gform_confirmation_anchor_6', '__return_false' );