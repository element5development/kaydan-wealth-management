<?php

/*----------------------------------------------------------------*\

	CUSTOM TAXONOMIES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Taxonomy Department
// Taxonomy Key: department
function create_department_tax() {

	$labels = array(
		'name'              => _x( 'Departments', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Department', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Departments', 'textdomain' ),
		'all_items'         => __( 'All Departments', 'textdomain' ),
		'parent_item'       => __( 'Parent Department', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Department:', 'textdomain' ),
		'edit_item'         => __( 'Edit Department', 'textdomain' ),
		'update_item'       => __( 'Update Department', 'textdomain' ),
		'add_new_item'      => __( 'Add New Department', 'textdomain' ),
		'new_item_name'     => __( 'New Department Name', 'textdomain' ),
		'menu_name'         => __( 'Department', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'department', array('teammember', ), $args );

}
add_action( 'init', 'create_department_tax' );

// Register Taxonomy Type
// Taxonomy Key: type
function create_type_tax() {

	$labels = array(
		'name'              => _x( 'Types', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Type', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Types', 'textdomain' ),
		'all_items'         => __( 'All Types', 'textdomain' ),
		'parent_item'       => __( 'Parent Type', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Type:', 'textdomain' ),
		'edit_item'         => __( 'Edit Type', 'textdomain' ),
		'update_item'       => __( 'Update Type', 'textdomain' ),
		'add_new_item'      => __( 'Add New Type', 'textdomain' ),
		'new_item_name'     => __( 'New Type Name', 'textdomain' ),
		'menu_name'         => __( 'Type', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'type', array('event', ), $args );

}
add_action( 'init', 'create_type_tax' );

//Sort Type archive
add_filter('pre_get_posts', 'sort_type_archive');
    function sort_type_archive($query) {
    if ( ( $query->is_main_query() ) && ( is_tax('type') ) ) {
			$query->set('meta_key', 'event_date');
			$query->set('orderby', 'meta_value');
			$query->set('order', 'ASC');
    }
    return $query;
}