<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/

function left_sidebar() {
	$args = array(
		'name'          => __( 'Left Sidebar' ),
		'id'            => 'left',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'left_sidebar' );

function right_sidebar() {
	$args = array(
		'name'          => __( 'Right Sidebar' ),
		'id'            => 'right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'right_sidebar' );

function left_footer_sidebar() {
	$args = array(
		'name'          => __( 'Left Footer Area' ),
		'id'            => 'left-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'left_footer_sidebar' );

function center_footer_sidebar() {
	$args = array(
		'name'          => __( 'Center Footer Area' ),
		'id'            => 'center-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'center_footer_sidebar' );

function right_footer_sidebar() {
	$args = array(
		'name'          => __( 'Right Footer Area' ),
		'id'            => 'right-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'right_footer_sidebar' );

function bottom_footer_sidebar() {
	$args = array(
		'name'          => __( 'Bottom Footer Area' ),
		'id'            => 'bottom-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'bottom_footer_sidebar' );