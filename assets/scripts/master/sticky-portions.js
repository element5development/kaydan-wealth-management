var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			STICKY AREAS
	\*----------------------------------------------------------------*/
	if ($(window).width() >= 1200) {
		$('#social-share-fixed').stickySidebar({
			topSpacing: 100,
			bottomSpacing: 100,
			stickyClass: 'is-sticky'
		});
	}
});