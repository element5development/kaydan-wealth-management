var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		INPUT ADDING AND REMVOING CLASSES
	\*----------------------------------------------------------------*/
	$('input:not([type=checkbox]):not([type=radio])').focus(function () {
		$(this).addClass('is-activated');
	});
	$('textarea').focus(function () {
		$(this).addClass('is-activated');
	});
	$('select').focus(function () {
		$(this).addClass('is-activated');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*------------------------------------------------------------------
		HAMBURGER
	------------------------------------------------------------------*/
	$(".hamburger-menu").click(function () {
		$(this).toggleClass("is-active");
		$('.navigation-block nav').toggleClass("is-active");
	});
	$(".navigation-block nav .close").on('click', function () {
		$(".hamburger-menu").removeClass("is-active");
		$('.navigation-block nav').removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'block');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*------------------------------------------------------------------
		SEARCH FORM
	------------------------------------------------------------------*/
	$("#search-button").click(function () {
		$("#search-form").addClass("is-active");
	});
	$("#search-close").click(function () {
		$("#search-form").removeClass("is-active");
	});
	/*------------------------------------------------------------------
		MOBILE MODAL
	------------------------------------------------------------------*/
	$("#modal-menu-close").click(function () {
		$(".hamburger-menu").removeClass("is-active");
		$('.navigation-block nav').removeClass("is-active");
	});
	/*------------------------------------------------------------------
		FOOTER COLLAPSE
	------------------------------------------------------------------*/
	$("#footer-close").click(function () {
		$(".page-footer").removeClass("is-active");
	});
	$("#footer-open").click(function () {
		$(".page-footer").addClass("is-active");
	});
	/*------------------------------------------------------------------
  	Sticky Primary Navigation
	------------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll > 40) {
			$('.navigation-block').addClass('is_stuck');
			$('.page-block').addClass('is_stuck');
		} else {
			$('.navigation-block').removeClass('is_stuck');
			$('.page-block').removeClass('is_stuck');
		}
	});
	/*------------------------------------------------------------------
  	APPROACH SLIDER
	------------------------------------------------------------------*/
	$('.approach-slider').slick({
		slidesToShow: 1,
		dots: true,
		infinite: false,
		adaptiveHeight: true,
	});
	/*------------------------------------------------------------------
  	SERVICES MOBILE SLIDER
	------------------------------------------------------------------*/
	function mobileOnlySlider() {
		$('.services-slider').slick({
			slidesToShow: 1,
			dots: true,
			infinite: false,
			adaptiveHeight: true,
		});
	}
	if (window.innerWidth < 1024) {
		mobileOnlySlider();
	}
	$(window).resize(function (e) {
		if (window.innerWidth < 1024) {
			if (!$('.services-slider').hasClass('slick-initialized')) {
				mobileOnlySlider();
			}

		} else {
			if ($('.services-slider').hasClass('slick-initialized')) {
				$('.services-slider').slick('unslick');
			}
		}
	});
	/*------------------------------------------------------------------
		MASONRY FOR WP GALLERY
	------------------------------------------------------------------*/
	$('.landscape').parent().addClass("landscape");
	var galleries = document.querySelectorAll('.gallery');
	for (var i = 0, len = galleries.length; i < len; i++) {
		var gallery = galleries[i];
		initMasonry(gallery);
	}

	function initMasonry(container) {
		var imgLoad = imagesLoaded(container, function () {
			new Masonry(container, {
				itemSelector: '.gallery-item',
				columnWidth: '.gallery-item:first-child',
				percentPosition: true,
				horizontalOrder: true
			});
		});
	}
	/*------------------------------------------------------------------
		CATEGORY CHIPS
	------------------------------------------------------------------*/
	$('.categories a').on('click', function () {
		$('.categories a').removeClass('is-active');
		$(this).addClass('is-active').siblings().removeClass('active');
	});
	/*------------------------------------------------------------------
		BOOK APPOINTMENT FORM
	------------------------------------------------------------------*/
	if (window.location.href.indexOf("?existing=yes") > -1) {
		$('#existing-client').addClass('is_hidden');
		$('#schedule-appointment').addClass('is_visible');
	} else if (window.location.href.indexOf("?existing=no") > -1) {
		$('#existing-client').addClass('is_hidden');
		$('#appointment-form').addClass('is_visible');
	}
	if (window.location.href.indexOf("?advisor=Kruzan") > -1) {
		$('#existing-client').addClass('is_hidden');
		$('#schedule-Kruzan').addClass('is_visible');
	} else if (window.location.href.indexOf("?advisor=Brennan") > -1) {
		$('#existing-client').addClass('is_hidden');
		$('#schedule-Brennan').addClass('is_visible');
	} else if (window.location.href.indexOf("?advisor=Houston") > -1) {
		$('#existing-client').addClass('is_hidden');
		$('#schedule-Houston').addClass('is_visible');
	}
	/*------------------------------------------------------------------
  	KAYDAN CARES SLIDER
	------------------------------------------------------------------*/
	$('.logo-slider').slick({
		slidesToShow: 3,
		infinite: true,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		prevArrow: $('.prev'),
		nextArrow: $('.next'),
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, ]
	});
	/*------------------------------------------------------------------
		BLOG POST FORM
	------------------------------------------------------------------*/
	$("#helpful-yes").on("click", function () {
		$('#content-helpful').removeClass('is_visible');
		$('#content-helpful').addClass('is_hidden');
		$('#content-helpful-form').removeClass('is_hidden');
		$('#content-helpful-form').addClass('is_visible');
	});
	$("#helpful-no").on("click", function () {
		$('#content-helpful').addClass('is_hidden');
		$('#content-helpful-related').addClass('is_visible');
	});
	$(".helpful-back").on("click", function () {
		$('#content-helpful-related').removeClass('is_visible');
		$('#content-helpful-related').addClass('is_hidden');
		$('#content-helpful-form').removeClass('is_hidden');
		$('#content-helpful-form').addClass('is_visible');
	});

});