<?php 
/*----------------------------------------------------------------*\

	SEARCH FORM
	This form is most commonly referanced in the navigation.php file.

\*----------------------------------------------------------------*/
?>

<form role="search" method="get" action="<?php echo get_site_url(); ?>">
  <label>Search</label>
  <input type="search" placeholder="<?php echo esc_attr_x( 'Press Enter to Search', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
  <button type="submit">Search</button>
</form>