<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<header class="page-title">
	<section class="block is-large-width has-standard-spacing">
		<div class="blocks">
			<h1>Search Results:</h1>
			<h3><?php echo get_search_query(); ?></h3>
		</div>
	</section>
</header>

<main>
	<a id="content" class="anchor"></a>

	<?php if (!have_posts()) : ?>
		<section class="is-standard-width has-standard-spacing">
			<h2>Sorry, we couldn't find what you were looking for</h2>
			<div id="search-form-no-results">
				<?php get_search_form(); ?>
			</div>
		</section>
	<?php endif; ?>

	<?php if ( have_posts() ) : ?>
		<section class="feed default-contents is-standard-width has-standard-spacing">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part('template-parts/previews/preview-post'); ?>
			<?php endwhile; ?>
		</section>
		<section class="infinite-scroll has-standard-spacing">
			<?php the_posts_pagination( array(
				'prev_text'	=> __( 'Previous page' ),
				'next_text'	=> __( 'Next page' ),
			) ); ?>
			<a class="button is-primary load-more">View more</a>
		</section>
	<?php else : ?>
		<!-- NO RESULTS FOUND -->
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/footers/footer'); ?>

<?php get_footer(); ?>