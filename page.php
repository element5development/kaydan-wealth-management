<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	Standard page template for website which should include all the
	commonly used options.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
			<?php if ( get_field('banner_title') ) : 
				get_template_part('template-parts/sections/banner');
			endif; ?>
			<?php if ( get_field('gallery') ) : 
				get_template_part('template-parts/sections/gallery');
			endif; ?>
			<?php if ( get_field('gallery') ) : 
				get_template_part('template-parts/sections/icon-slider');
			endif; ?>
		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>