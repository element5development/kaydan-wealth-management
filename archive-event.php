<?php 
/*----------------------------------------------------------------*\

	EVENTS ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');

	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php get_template_part('template-parts/headers/header-archives'); ?>

	<section class="categories is-standard-width has-small-spacing">
		<h2>Categories</h2>

		<?php wp_nav_menu(array( 'theme_location' => 'event_type_nav' )); ?>

	</section>

	<main>
		<a id="content" class="anchor"></a>
		<?php if ( have_posts() ) : ?>
			<?php if ( get_field( $posttype . '_editor', 'option') ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_field( $posttype . '_editor', 'option'); ?>
				</section>
			<?php endif; ?>

			<section class="event-feed grid has-two-column is-standard-width has-standard-spacing">
				<div class="calendar-wrap">
					<script>
						var events = [
							<?php while ( have_posts() ) : the_post(); ?>
								{
									Title: "<?php the_title(); ?>",
									Date: new Date("<?php the_field('event_date'); ?>"),
									excerpt:"<?php the_field('event_description'); ?>",
									shortdate:"<?php the_field('event_date'); ?>",
									time:"<?php the_field('start_time'); ?> - <?php the_field('end_time'); ?>",
									place:"<?php the_field('event_location'); ?><br><?php the_field('address'); ?><br><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?>",
								},
							<?php endwhile; ?>
						];
						var $ = jQuery;
						$(document).ready(function () {
							$(document).on('click', '.close', function() {
								$(".over").fadeOut();
							});
							$("#calendar").datepicker({
								beforeShowDay: function(date) {
									var result = [true, '', null];
									var matching = $.grep(events, function(event) {
										return event.Date.valueOf() === date.valueOf();
									});
									if (matching.length) {
										result = [true, 'highlight', null];
									}
									return result;
								},
								onChangeMonthYear: addCustomInformation,
								onSelect: function(dateText) {
								var date,
									selectedDate = new Date(dateText),
									i = 0,
									event = null;
									while (i < events.length && !event) {
										date = events[i].Date;
										if (selectedDate.valueOf() === date.valueOf()) {
											event = events[i];
										}
										i++;
									}
									if (event) {
										$(".eventdata").empty();
										$(".over").fadeIn("slow");
										$(".eventdata").append("<p>"+event.shortdate+"</p><h1>"+event.Title+"</h1><p>"+event.excerpt+"</p><h2>"+event.time+"</h2><h3>"+event.place+"</h3>");
									}
									addCustomInformation();
								},
							});
							addCustomInformation();
							$('#prev svg').on('click', function (e) {
								$('.ui-datepicker-prev').trigger("click");
							});
							$('#next svg').on('click', function (e) {
								$('.ui-datepicker-next').trigger("click");
							});
						});

						function addCustomInformation() {
							setTimeout(function() {
								$(".ui-datepicker-year").after("<span> Events</span>");
							}, 0)
						}
					</script>
					<div id="calendar">
						<div class="over">
							<div class="eventdata"></div>
							<a class="close" href="javascript:;">X</a>
						</div>
						<button id="prev">
							<svg viewBox="0 0 64 64">
								<path data-name="layer1" fill="none" stroke="#c7995e" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linecap="round"></path>
							</svg>
						</button>
						<button id="next">
							<svg viewBox="0 0 64 64">
								<path data-name="layer1" fill="none" stroke="#c7995e" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linecap="round"></path>
							</svg>
						</button>
					</div>
				</div>
				<div class="featured-event">
					<h3>Featured Event</h3>

					<?php 
						$featured_posts = get_posts(array(
							'post_type' 			=> 'event',
							'posts_per_page' 	=> 1,
							'meta_key'				=> 'event_date',
							'orderby'					=> 'meta_value',
							'order'           => 'ASC',
							'meta_query' 			=> array(
								array(
									'key' 			=> 'featured_event',
									'compare' 	=> '=',
									'value' 		=> '1'
								)
							)
						));
					?>
					<?php if( $featured_posts ): ?>
						<?php foreach( $featured_posts as $post ): setup_postdata( $post ) ?>
							<article class="preview preview-post event-preview">
								<div class="categories">
									<?php 
										$categories = get_the_terms( $post->ID , 'type' );
										if ( ! empty( $categories ) ) {
											foreach( $categories as $category ) {
												$category_link = get_term_link( $category, 'type' );
												echo '<a href="' . $category_link . '">' . $category->name . '</a> ';
											}
										}
									?>
								</div>
								<?php if ( get_field('event_date') ) : ?>
									<p class="has-date"><?php the_field('event_date'); ?></p>
								<?php endif; ?>
								<h1 class="has-subheader"><?php the_title(); ?></h1>
								<?php if ( get_field('event_description') ) : ?>
									<p class="subheader"><?php the_field('event_description'); ?></p>
								<?php endif; ?>
								<?php if ( get_field('start_time') ) : ?>
									<h2><?php the_field('start_time'); ?> - <?php the_field('end_time'); ?></h2>
								<?php endif; ?>
								<?php if ( get_field('event_location') ) : ?>
									<h3><?php the_field('event_location'); ?><br><?php the_field('address'); ?><br><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?></h3>
								<?php endif; ?>
								<div class="buttons">
									<?php
									$link = get_field('event_link');
									if( $link ): ?>
										<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
										<div title="Add to Calendar" class="addeventatc">
											Add to my calendar
											<?php 
												$startdate = get_field('event_date', false, false);
												$startdate = new DateTime($startdate);
											?>
											<span class="start"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('start_time'); ?></span>
											<span class="end"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('end_time'); ?></span>
											<span class="timezone">America/Detroit</span>
											<span class="title"><?php the_title(); ?></span>
											<span class="description"><?php the_field('event_description'); ?></span>
											<span class="location"><?php the_field('event_location'); ?></span>
										</div>
									<?php endif; ?>
								</div>
							</article>
						<?php endforeach; ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<article class="preview preview-post event-preview">
							<h1>We have no featured events at this time</h1>
						</article>
					<?php endif; ?>
				</div>
			</section>

			<section class="event-feed grid has-two-column is-standard-width">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php 
						$currentdate = date('Ymd');
						$eventdate = get_field('event_date', false, false);
					?>
					<?php if( $eventdate >= $currentdate ): ?>
						<?php get_template_part('template-parts/previews/preview-' . $posttype); ?>
					<?php endif; ?>
				<?php endwhile; ?>
			</section>
			<section class="infinite-scroll is-standard-width has-small-spacing">
				<div class="page-load-status">
					<p class="infinite-scroll-request">
						<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
							<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
								<animateTransform attributeType="xml"
									attributeName="transform"
									type="rotate"
									from="0 25 25"
									to="360 25 25"
									dur="0.6s"
									repeatCount="indefinite"/>
							</path>
						</svg>
					</p>
					<p class="infinite-scroll-last"></p>
					<p class="infinite-scroll-error"></p>
				</div>
				<?php $pagination = the_posts_pagination( array(
					'prev_text'	=> __( 'Previous page' ),
					'next_text'	=> __( 'Next page' ),
				) ); ?>
				<button class="load-more">View more</button>
			</section>
		<?php else : ?>
			<!-- NO RESULTS FOUND -->
		<?php endif; ?>
	</main>

	<?php get_template_part('template-parts/footers/footer'); ?>

</div>

<?php get_footer(); ?>