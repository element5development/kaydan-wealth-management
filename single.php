<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php get_template_part('template-parts/headers/header-post'); ?>

	<main>
		<a id="content" class="anchor"></a>

		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">

					<?php 
						$author = get_the_author(); 
						$authorlink = str_replace(" ", "-", $author);
					?>

					<p>by: <a href="<?php echo get_site_url(); ?>/registered-investment-advisors/<?php echo strtolower($authorlink); ?>/"><?php echo $author; ?></a></p>
					<?php the_content(); ?>

					<?php the_tags( '<ul class="post-tags"><li>#', '</li><li>#', '</li></ul>' ); ?>
				</section>
			<?php endif; ?>
			<?php get_template_part('template-parts/elements/share'); ?>
		</article>
		<div class="related-wrap">
			<section class="is-large-width has-large-spacing">
				<div id="content-helpful" class="blocks">
					<h3>Did you find this content helpful?</h3>
					<form>
						<ul class="gfield_checkbox">
							<li>
								<input name="helpful" type="radio" id="helpful-yes" value="yes" required>
								<label for="yes">Yes</label>
							</li>
							<li>
								<input name="helpful" type="radio" id="helpful-no" value="no" required>
								<label for="no">No</label>
							</li>
						</ul>
					</form>
				</div>
				<div id="content-helpful-form" class="blocks">
					<h3>Awesome, fill out this form and we’ll send you more information.</h3>
					<?php echo do_shortcode('[gravityform id=7 title=false description=false]'); ?>
				</div>
				<div id="content-helpful-related" class="blocks">
					<?php get_template_part('template-parts/sections/related'); ?>
				</div>
			</section>
		</div>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>