<?php 
/*----------------------------------------------------------------*\

	Template Name: Glossary

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>

			<section class="is-standard-width has-standard-spacing half">
				<div><?php the_field('left_content'); ?></div>
				<div><?php the_field('right_content'); ?></div>
			</section>

			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>

			<section class="is-standard-width has-small-spacing glossary">

				<div id="letter-wrap" class="letters-wrap">
					<div class="letters">
						<a href="#a" class="smooth-scroll">A</a>
						<p>|</p>
						<a href="#b" class="smooth-scroll">B</a>
						<p>|</p>
						<a href="#c" class="smooth-scroll">C</a>
						<p>|</p>
						<a href="#d" class="smooth-scroll">D</a>
						<p>|</p>
						<a href="#e" class="smooth-scroll">E</a>
						<p>|</p>
						<a href="#f" class="smooth-scroll">F</a>
						<p>|</p>
						<a href="#g" class="smooth-scroll">G</a>
						<p>|</p>
						<a href="#h" class="smooth-scroll">H</a>
						<p>|</p>
						<a href="#i" class="smooth-scroll">I</a>
						<p>|</p>
						<a href="#j" class="smooth-scroll">J</a>
						<p>|</p>
						<a href="#k" class="smooth-scroll">K</a>
						<p>|</p>
						<a href="#l" class="smooth-scroll">L</a>
						<p>|</p>
						<a href="#m" class="smooth-scroll">M</a>
						<p>|</p>
					</div>
					<div class="letters">
						<a href="#n" class="smooth-scroll">N</a>
						<p>|</p>
						<a href="#o" class="smooth-scroll">O</a>
						<p>|</p>
						<a href="#p" class="smooth-scroll">P</a>
						<p>|</p>
						<a href="#q" class="smooth-scroll">Q</a>
						<p>|</p>
						<a href="#r" class="smooth-scroll">R</a>
						<p>|</p>
						<a href="#s" class="smooth-scroll">S</a>
						<p>|</p>
						<a href="#t" class="smooth-scroll">T</a>
						<p>|</p>
						<a href="#u" class="smooth-scroll">U</a>
						<p>|</p>
						<a href="#v" class="smooth-scroll">V</a>
						<p>|</p>
						<a href="#w" class="smooth-scroll">W</a>
						<p>|</p>
						<a href="#x" class="smooth-scroll">X</a>
						<p>|</p>
						<a href="#y" class="smooth-scroll">Y</a>
						<p>|</p>
						<a href="#z" class="smooth-scroll">Z</a>
					</div>
				</div>

				<?php if( have_rows('letter_repeater') ): ?>
					<?php while ( have_rows('letter_repeater') ) : the_row(); ?>
					
						<?php $letter = get_sub_field('letter'); ?>

						<div id="<?php echo (str_replace(' ', '-', strtolower($letter))); ?>" class="letter-wrap">
							<h2><?php the_sub_field('letter'); ?></h2>

							<?php if( have_rows('term_repeater') ): ?>
								<?php while ( have_rows('term_repeater') ) : the_row(); ?>

									<?php $term = get_sub_field('term'); ?>

									<div id="<?php echo (str_replace(array("'", "(", ")"), "", (str_replace(' ', '-', strtolower($term))))); ?>">
										<h3><?php the_sub_field('term'); ?></h3>
										<?php the_sub_field('definition'); ?>
									</div>

								<?php endwhile; ?>
							<?php endif; ?>

							<a class="button is-primary is-ghost smooth-scroll" href="#letter-wrap">Back to top</a>
						</div>

					<?php endwhile; ?>
				<?php endif; ?>

			</section>

			<section class="is-large-width has-standard-spacing">
				<p>Source: Raymond James</p>
				<p class="disclaimer"><?php the_field('disclaimer_text'); ?></p>
			</section>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>