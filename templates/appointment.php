<?php 
/*----------------------------------------------------------------*\

	Template Name: Appointment

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<header class="page-title has-image" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
		<section class="block is-standard-width has-standard-spacing">

			<div id="existing-client" class="blocks">
				<h1>Are you an existing client?</h1>
				<form>
					<ul class="gfield_checkbox">
						<li>
							<input name="existing" type="radio" id="yes" value="yes" required>
							<label for="yes">Yes, I'm an existing client</label>
						</li>
						<li>
							<input name="existing" type="radio" id="no" value="no" required>
							<label for="no">No, I'm not an existing client</label>
						</li>
					</ul>
					<button class="is-massive">Next</button>
				</form>
				<p>Not yet. <a href="<?php echo get_site_url(); ?>/approach/">Show me how you do this</a>.</p>
			</div>
			<div id="schedule-appointment" class="blocks">
				<a href="<?php echo get_site_url(); ?>/book-an-appointment/" class="back">Back</a>
				<h2>Who is your advisor?</h2>
				<form>
					<ul class="gfield_checkbox">
						<li>
							<input name="advisor" type="radio" id="Kruzan" value="Kruzan" required>
							<label for="Kruzan">James B. Kruzan, CFP®, CRPC® is my advisor</label>
						</li>
						<li>
							<input name="advisor" type="radio" id="Brennan" value="Brennan" required>
							<label for="Brennan">Darren Brennan, CFP®, CRPC® is my advisor</label>
						</li>
						<li>
							<input name="advisor" type="radio" id="Houston" value="Houston" required>
							<label for="Houston">Kimberly Houston, CFP®, CRPC® is my advisor</label>
						</li>
					</ul>
					<button class="is-massive">Schedule Appointment</button>
				</form>
				<h4>Is there another team member who can answer your question?</h4>
				<p>For common requests that do not require an appointment with your advisor view our <a href="<?php echo get_site_url(); ?>/faq/">FAQs</a>.</p>
				<p class="disclaimer">Disclosure: Certified Financial Planner Board of Standards Inc. owns the certification marks CFP®, CERTIFIED FINANCIAL PLANNER TM and CFP® in the U.S.</p>
			</div>
			<div id="schedule-Kruzan" class="blocks">
				<a href="<?php echo get_site_url(); ?>/book-an-appointment/?existing=yes" class="back">Back</a>
				<!-- Calendly inline widget begin -->
				<div class="calendly-inline-widget" data-url="https://calendly.com/james-kruzan" style="min-width:320px;height:650px;"></div>
				<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
				<!-- Calendly inline widget end -->
			</div>
			<div id="schedule-Brennan" class="blocks">
				<a href="<?php echo get_site_url(); ?>/book-an-appointment/?existing=yes" class="back">Back</a>
				<!-- Calendly inline widget begin -->
				<div class="calendly-inline-widget" data-url="https://calendly.com/darren-brennan" style="min-width:320px;height:580px;"></div>
				<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
				<!-- Calendly inline widget end -->
			</div>
			<div id="schedule-Houston" class="blocks">
				<a href="<?php echo get_site_url(); ?>/book-an-appointment/?existing=yes" class="back">Back</a>
				<!-- Calendly inline widget begin -->
				<div class="calendly-inline-widget" data-url="https://calendly.com/kimberlyhouston" style="min-width:320px;height:580px;"></div>
				<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
				<!-- Calendly inline widget end -->
			</div>
			<div id="appointment-form" class="blocks">
				<a href="<?php echo get_site_url(); ?>/book-an-appointment/" class="back">Back</a>
				<?php echo do_shortcode('[gravityform id=6 title=false description=false]'); ?>
			</div>

		</section>

	</header>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>