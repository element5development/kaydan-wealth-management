<?php 
/*----------------------------------------------------------------*\

	Template Name: Contact

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>

			<div class="form-section-wrap">
				<section class="form has-standard-spacing">
					<?php the_field('left_content'); ?>
				</section>
				<section class="book-appointment has-standard-spacing">
					<div>
						<?php the_field('right_content'); ?>
					</div>
				</section>
			</div>

			<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="main-content-block is-standard-width has-small-spacing">
				<?php the_content(); ?>
			</section>
			<?php endif; ?>

			<?php if( have_rows('department_repeater') ): ?>
			<?php while ( have_rows('department_repeater') ) : the_row(); ?>

			<section class="is-standard-width has-small-spacing">

				<h3><?php the_sub_field('department_title'); ?></h3>

				<?php if( have_rows('department_list') ): ?>

				<ul>
					<?php while ( have_rows('department_list') ) : the_row(); ?>

					<li><?php the_sub_field('department_content'); ?></li>

					<?php endwhile; ?>
				</ul>

				<?php endif; ?>

				<?php 
					$posts = get_sub_field('department_select');

					if( $posts ): ?>
				<section class="team feed grid has-two-column is-standard-width has-standard-spacing">
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					<?php setup_postdata($post); ?>
					<?php get_template_part( 'template-parts/previews/preview-teammember-small' ); ?>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				</section>
				<?php endif; ?>

			</section>

			<?php endwhile; ?>
			<?php endif; ?>

			<section class="is-standard-width text-center">

				<?php $link = get_field('department_cta'); ?>

				<?php if( !empty($link) ): ?>
				<a class="button is-primary is-massive" href="<?php echo $link['url']; ?>"
					target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
				<?php endif; ?>

			</section>

			<?php if( have_rows('location_repeater') ): ?>

			<section class="location-title is-standard-width has-small-spacing">
				<h2 id="locations">Locations</h2>
			</section>
			<section class="locations grid has-two-column is-standard-width has-small-spacing">

				<?php while ( have_rows('location_repeater') ) : the_row(); ?>

				<div>
					<?php the_sub_field('map'); ?>
					<h3><?php the_sub_field('location_name'); ?></h3>
					<p><?php the_sub_field('address'); ?><br><?php the_sub_field('city'); ?>, <?php the_sub_field('state'); ?>
						<?php the_sub_field('zip_code'); ?></p>

					<?php $link = get_sub_field('primary_phone'); ?>

					<?php if( !empty($link) ): ?>
					<a href="<?php echo $link['url']; ?>"
						target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>

					<?php $link2 = get_sub_field('secondary_phone'); ?>

					<?php if( !empty($link2) ): ?>
					<a href="<?php echo $link2['url']; ?>"
						target="<?php echo $link2['target']; ?>"><?php echo $link2['title']; ?></a>
					<?php endif; ?>

					<?php $link3 = get_sub_field('fax'); ?>

					<?php if( !empty($link3) ): ?>
					<a href="<?php echo $link3['url']; ?>"
						target="<?php echo $link3['target']; ?>"><?php echo $link3['title']; ?></a>
					<?php endif; ?>

					<h4>Office Hours</h4>
					<p><?php the_sub_field('hours'); ?></p>

					<h4>Appointments</h4>
					<p><?php the_sub_field('appointments'); ?></p>

					<?php $link4 = get_sub_field('location_cta'); ?>

					<?php if( !empty($link4) ): ?>
					<a class="button is-primary" href="<?php echo $link4['url']; ?>"
						target="<?php echo $link4['target']; ?>"><?php echo $link4['title']; ?></a>
					<?php endif; ?>
				</div>

				<?php endwhile; ?>

			</section>

			<?php endif; ?>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>