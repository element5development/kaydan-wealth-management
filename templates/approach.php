<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>

			<section class="is-standard-width has-standard-spacing">

				<?php if( have_rows('slide_repeater') ): ?>
					<div class="approach-slider">
						<?php while ( have_rows('slide_repeater') ) : the_row(); ?>

							<div class="approach-slide">
								<h2><?php the_sub_field('title'); ?></h2>

								<?php if( have_rows('card_repeater') ): ?>
									<?php while ( have_rows('card_repeater') ) : the_row(); ?>
									
									<div class="approach-card">

										<?php $image = get_sub_field('card_icon'); ?>

										<?php if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>

										<p><strong><?php the_sub_field('card_title'); ?></strong></p>
										<?php the_sub_field('card_description'); ?>

										<?php if( have_rows('card_pdf_link_repeater') ): ?>
											<?php while ( have_rows('card_pdf_link_repeater') ) : the_row(); ?>

												<?php $file = get_sub_field('file_link'); ?>

												<?php if( !empty($file) ): ?>
													<p class="resources">
														<a href="<?php echo $file['url']; ?>" target="_blank">
															<svg>
																<use xlink:href="#file"></use>
															</svg>
															<?php echo $file['title']; ?>
														</a>
													</p>
												<?php endif; ?>
												

											<?php endwhile; ?>
										<?php endif; ?>

									</div>

									<?php endwhile; ?>
								<?php endif; ?>
							</div>

						<?php endwhile; ?>
					</div>
				<?php endif; ?>

			</section>
			<hr>
			<section class="is-standard-width has-standard-spacing">
				<?php the_field('after_slider_content'); ?>
			</section>
			<?php if ( get_field('banner_title') ) : 
				get_template_part('template-parts/sections/banner');
			endif; ?>
		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>