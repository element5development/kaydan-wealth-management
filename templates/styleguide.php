<?php 
/*-------------------------------------------------------------------
    Template Name: Styleguide
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main class="styleguide-container">
		<a id="content" class="anchor"></a>
		<div class="brand block">
			<!--  COLORS  -->
			<section class="styleguide-section is-large-width">
				<h2>Colors palette</h2>
				<div class="colors">
					<p>Primary Colors</p>
					<div class="color">
						<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
							<p>COPIED!</p>
						</a>
						<div class="color-details">
							<p class="hex">HEX<span></span></p>
							<p class="rgb">RGB<span></span></p>
							<p class="cmyk">CMYK<span></span></p>
						</div>
					</div>
					<div class="color">
						<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
							<p>COPIED!</p>
						</a>
						<div class="color-details">
							<p class="hex">HEX<span></span></p>
							<p class="rgb">RGB<span></span></p>
							<p class="cmyk">CMYK<span></span></p>
						</div>
					</div>
					<div class="color">
						<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
							<p>COPIED!</p>
						</a>
						<div class="color-details">
							<p class="hex">HEX<span></span></p>
							<p class="rgb">RGB<span></span></p>
							<p class="cmyk">CMYK<span></span></p>
						</div>
					</div>
					<div class="color">
						<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
							<p>COPIED!</p>
						</a>
						<div class="color-details">
							<p class="hex">HEX<span></span></p>
							<p class="rgb">RGB<span></span></p>
							<p class="cmyk">CMYK<span></span></p>
						</div>
					</div>
					<div class="color">
						<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
							<p>COPIED!</p>
						</a>
						<div class="color-details">
							<p class="hex">HEX<span></span></p>
							<p class="rgb">RGB<span></span></p>
							<p class="cmyk">CMYK<span></span></p>
						</div>
					</div>
				</div>
			</section>
			<!--  FONTS -->
			<section class="styleguide-section is-large-width">
				<h2>FONTS</h2>
				<div class="fonts">
					<div class="font">
						<div class="block">
							<p class="family"></p>
							<p class="example">Aa</p>
							<p class="weight"></p>
						</div>
					</div>
					<div class="font">
						<div class="block">
							<p class="family"></p>
							<p class="example">Aa</p>
							<p class="weight"></p>
						</div>
					</div>
					<div class="font">
						<div class="block">
							<p class="family"></p>
							<p class="example">Aa</p>
							<p class="weight"></p>
						</div>
					</div>
					<div class="font">
						<div class="block">
							<p class="family"></p>
							<p class="example">Aa</p>
							<p class="weight"></p>
						</div>
					</div>
				</div>
			</section>
			<!--  TYPOGRAPHY -->
			<section class="styleguide-section is-large-width">
				<h2>TYPOGRAPHY</h2>
				<div class="headings">
					<div class="typography">
						<p>H1</p>
						<h1>This is a headline</h1>
					</div>
					<div class="typography">
						<p>H2</p>
						<h2>This is a headline</h2>
					</div>
					<div class="typography">
						<p>H3</p>
						<h3>This is a headline</h3>
					</div>
					<div class="typography">
						<p>H4</p>
						<h4>This is a headline</h4>
					</div>
					<div class="typography">
						<p>Paragraph</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet pharetra metus. Sed a purus massa. Cras tempus pharetra quam, nec mattis dui semper et. Nunc sapien arcu, bibendum a euismod eget, facilisis quis nisl.</p>
					</div>
				</div>
			</section>
			<!--  LOGOS  -->
			<section class="styleguide-section is-large-width">
				<h2>Logo</h2>
				<div class="logos">
					<?php if( have_rows('logo_variant') ):
						while ( have_rows('logo_variant') ) : the_row(); ?>
							<div class="logo" style="background-color: <?php the_sub_field('background_color'); ?>">
							<?php $logo_variant = get_sub_field('logo'); ?>
							<a target="_blank" href="<?php echo $logo_variant['url']; ?>">
								<img src="<?php echo $logo_variant['url']; ?>" alt="<?php echo $logo_variant['alt']; ?>" />
							</a>
							</div>
						<?php endwhile;
					endif; ?>
				</div>
			</section>
			<!--  BUTTONS -->
			<section class="styleguide-section is-large-width">
				<h2>Buttons</h2>
				<div class="buttons">
					<p>Primary</p>
					<a href="" class="button">button</a>
					<a href="" class="button is-hover">hover</a>
					<a href="" class="button is-active">active</a>
				</div>
				<div class="buttons">
					<p>Secondary</p>
					<a href="" class="button is-secondary">button</a>
					<a href="" class="button is-secondary is-hover">hover</a>
					<a href="" class="button is-secondary is-active">active</a>
				</div>
			</section>
			<!-- LISTS -->
			<section class="styleguide-section is-large-width">
				<h2>Lists</h2>
				<div class="lists">
					<div class="list">
						<ul>
							<li>Unordered List</li>
							<li>Unordered List</li>
							<li>Unordered List</li>
						</ul>
					</div>
				</div>
			</section>
			<!-- BLOCKQUOTE -->
			<section class="styleguide-section is-large-width">
				<h2>Blockquote</h2>
				<div class="quotes">
					<blockquote>
						<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu condimentum ex, non tincidunt enim."</p>
					</blockquote>
				</div>
			</section>
		</div>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>