<?php 
/*----------------------------------------------------------------*\

	Template Name: Employees

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php if ( ! is_user_logged_in() ) { // Display WordPress login form: ?>

		<header class="page-title has-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-header.jpg');">
			<section class="login is-standard-width has-standard-spacing">
				<h2>Employees Login</h2>
				<p>Access the Employee Calendar and the complete list of employee resources.</p>

				<?php	$args = array(
					'redirect' => admin_url(), 
					'form_id' => 'loginform-custom',
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_log_in' => __( 'Login' ),
					'remember' => false
					);
					wp_login_form( $args ); ?>
			</section>
		</header>

	<?php	} else { // If logged in: ?>
	<?php
		$user = wp_get_current_user();
		$allowed_roles = array( 'editor', 'administrator', 'author', 'subscriber', 'contributor' );
		if ( array_intersect( $allowed_roles, $user->roles ) ) {
	?>
		<header class="page-title has-image" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
			<section class="block is-large-width has-standard-spacing">
				<div class="blocks">
					<h1>Employees</h1>
					<h3>Of Kaydan Wealth Management</h3>
				</div>
			</section>
		</header>
		<main>
			<a id="content" class="anchor"></a>
			<section class="is-standard-width has-small-spacing">
				<h2>Employee Calendar</h2>
			</section>
			<section class="grid has-two-column is-standard-width has-small-spacing">
				<div class="calendar-wrap">
					<script>
						var events = [

							<?php //QUERY TEAMMEMBERS
								$args = array( 
									'posts_per_page'  => -1, 
									'post_type' 			=> 'teammember',
								);
								$team_query = new WP_Query( $args );
							?>
							<?php if ( $team_query->have_posts() ) : ?>
								<?php while ( $team_query->have_posts() ) : $team_query->the_post(); ?>
									
									<?php if( have_rows('time_off_repeater') ): ?>
										<?php while ( have_rows('time_off_repeater') ) : the_row(); ?>

											<?php $timeoff = get_sub_field('time_off'); ?>

											{
												Date: new Date("<?php echo $timeoff; ?>"),
												name:"<?php the_title(); ?>",
												timeoff:"<?php echo $timeoff; ?>",
											},

										<?php endwhile; ?>
									<?php endif; ?>

								<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>

						];
						var $ = jQuery;
						$(document).ready(function () {
							$(document).on('click', '.close', function() {
								$(".over").fadeOut();
							});
							$("#calendar").datepicker({
								beforeShowDay: function (date) {
									var result = [true, '', null];
									var matching = $.grep(events, function (event) {
										return event.Date.valueOf() === date.valueOf();
									});

									if (matching.length) {
										result = [true, 'highlight', null];
									}
									return result;
								},
								onSelect: function(dateText) {
								var date,
									selectedDate = new Date(dateText),
									i = 0,
									event = null;
									while (i < events.length && !event) {
										date = events[i].Date;
										if (selectedDate.valueOf() === date.valueOf()) {
											event = events[i];
										}
										i++;
									}
									if (event) {
										$(".eventdata").empty();
										$(".over").fadeIn("slow");
										<?php //QUERY TEAMMEMBERS
											$args = array( 
												'posts_per_page'  => -1, 
												'post_type' 			=> 'teammember',
											);
											$team_query = new WP_Query( $args );
										?>
										<?php if ( $team_query->have_posts() ) : ?>
											<?php while ( $team_query->have_posts() ) : $team_query->the_post(); ?>
												
												<?php if( have_rows('time_off_repeater') ): ?>
													<?php while ( have_rows('time_off_repeater') ) : the_row(); ?>

													<?php $timeoff = get_sub_field('time_off'); ?>

														if( dateText.valueOf() == "<?php echo $timeoff; ?>" ) {

															<?php $username = get_the_title(); ?>
													
															$('.eventdata').each(function(){
																var username = "<?php echo $username; ?>";
																$(this).append("<h1>" + username + "</h1>");
															});

														}

													<?php endwhile; ?>
												<?php endif; ?>

											<?php endwhile; ?>
										<?php endif; ?>
										<?php wp_reset_query(); ?>
									}
								},
							});
							$('#prev').on('click', function (e) {
								$('.ui-datepicker-prev').trigger("click");
							});
							$('#next').on('click', function (e) {
								$('.ui-datepicker-next').trigger("click");
							});
						});
					</script>
					<div id="calendar">
						<div class="over">
							<div class="eventdata"></div>
							<a class="close" href="javascript:;">X</a>
						</div>
						<button id="prev">
							<svg viewBox="0 0 64 64">
								<path data-name="layer1" fill="none" stroke="#c7995e" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linecap="round"></path>
							</svg>
						</button>
						<button id="next">
							<svg viewBox="0 0 64 64">
								<path data-name="layer1" fill="none" stroke="#c7995e" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linecap="round"></path>
							</svg>
						</button>
					</div>
				</div>
				<div class="upload-form">
					<h3>Upload HR Documents</h3>
					<p>You can upload and send your vactation requests and other HR documents here.</p>
					<?php echo do_shortcode('[gravityform id=8 title=false description=false]'); ?>
				</div>
			</section>
			<section class="is-standard-width has-standard-spacing section-links">
				<h2>Resources for you</h2>

				<?php if( have_rows('link_repeater') ): ?>

					<ul>

						<?php while ( have_rows('link_repeater') ) : the_row(); ?>

							<?php $link = get_sub_field('tool_link'); ?>

							<?php if( $link ): ?>
								<li>
									<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
										<?php echo $link['title']; ?>
									</a>
								</li>
							<?php endif; ?>

						<?php endwhile; ?>

					</ul>

				<?php endif; ?>
				
			</section>
			<section class="is-standard-width has-standard-spacing section-links">
				<h2>Forms & Documents</h2>

				<?php if( have_rows('resource_repeater') ): ?>
					<?php while ( have_rows('resource_repeater') ) : the_row(); ?>

						<?php $file = get_sub_field('resource'); ?>

						<?php if( !empty($file) ): ?>
							<p>
								<a class="button is-primary" href="<?php echo $file['url']; ?>" target="_blank">
									<?php echo $file['title']; ?>
								</a>
							</p>
						<?php endif; ?>

					<?php endwhile; ?>
				<?php endif; ?>
				
			</section>
			<section class="is-standard-width has-small-spacing">
				<?php	wp_loginout( home_url() ); // Display "Log Out" link. ?>
			</section>
		</main>
	<?php	} else { // If no role: ?>
	<header class="page-title has-image" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
		<section class="block is-large-width has-standard-spacing">
			<div class="blocks">
				<h1>Employees</h1>
				<h3>Of Kaydan Wealth Management</h3>
			</div>
		</section>
	</header>
	<main>
		<a id="content" class="anchor"></a>
		<section class="is-standard-width has-small-spacing">
			<h2>You do not have proper access to view this content.</h2>
			<br>
		</section>
	</main>
	<?php } ?>
	<?php	} ?>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>