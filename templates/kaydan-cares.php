<?php 
/*----------------------------------------------------------------*\

	Template Name: Kaydan Cares

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php get_template_part('template-parts/headers/header-kaydan-cares'); ?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-small-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>

			<div class="count-up">
				<section class="is-large-width has-standard-spacing">
					<div>
						<svg>
							<use xlink:href="#clock"></use>
						</svg>
						<?php $time = get_field('time_donated'); ?>
						<h3><span class="counter" data-count="<?php echo $time; ?>">0</span> hrs</h3>
						<P>Time Donated</p>
					</div>
					<div>
						<svg>
							<use xlink:href="#present"></use>
						</svg>
						<?php $items = get_field('items_donated'); ?>
						<h3><span class="counter" data-count="<?php echo $items; ?>">0</span></h3>
						<P>Items Donated</p>
					</div>
					<div>
						<svg>
							<use xlink:href="#money"></use>
						</svg>
						<?php $dollars = get_field('dollars_donated'); ?>
						<h3>$<span class="counter" data-count="<?php echo (str_replace(',', '', $dollars)); ?>">0</span></h3>
						<P>Dollars Donated</p>
					</div>
				</section>
			</div>

			<section class="logo-slider-section is-large-width has-standard-spacing">
				<h3>Kaydan Cares Supports These Great Organizations</h3>

				<?php if( have_rows('kaydan_cares_organization_repeater') ): ?>

					<div class="logo-slider">

						<?php while ( have_rows('kaydan_cares_organization_repeater') ) : the_row(); ?>

							<?php $image = get_sub_field('organization_logo'); ?>
							<?php $link = get_sub_field('organization_link'); ?>

							<?php if( !empty($link) ): ?>
								<div>
									<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								</div>
							<?php endif; ?>

						<?php endwhile; ?>

					</div>
					<button class="prev">
						<svg viewBox="0 0 64 64">
							<path data-name="layer1" fill="none" stroke="#2C517C" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linecap="round"/>
						</svg>
					</button>
					<button class="next">
						<svg viewBox="0 0 64 64">
							<path data-name="layer1" fill="none" stroke="#2C517C" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linecap="round"/>
						</svg>
					</button>

				<?php endif; ?>

			</section>

			<section class="feed grid has-three-column is-large-width has-standard-spacing">
				<?php //QUERY KAYDAN CARES POSTS
					$args = array( 
						'cat' => 36, 
						'posts_per_page'  => -1,
					);
					$kc_query = new WP_Query( $args );
				?>
				<?php if ( $kc_query->have_posts() ) : ?>
					<?php while ( $kc_query->have_posts() ) : $kc_query->the_post(); ?>
						<?php get_template_part( 'template-parts/previews/preview-blog' ); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</section>

			<section class="is-large-width has-standard-spacing">
				<p class="disclaimer"><?php the_field('disclaimer_text'); ?></p>
			</section>
		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>