<?php 
/*----------------------------------------------------------------*\

	Template Name: Thank You
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<section class="main-content-block is-standard-width has-standard-spacing">
				<h1 class="has-subheader"><?php the_field('page_title'); ?></h1>
				<p class="subheader"><?php the_field('title_description'); ?></p>
				<div class="buttons">
					<a class="button is-primary is-massive" href="/">Return to the Homepage</a>
				</div>
			</section>
		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>