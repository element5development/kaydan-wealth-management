<?php 
/*----------------------------------------------------------------*\

	Template Name: Payment

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<header class="page-title has-image" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
		<section class="block is-standard-width has-standard-spacing">
			<?php if ( '' !== get_post()->post_content ) : ?>
				<?php the_content(); ?>
			<?php endif; ?>
		</section>

	</header>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>