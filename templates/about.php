<?php 
/*----------------------------------------------------------------*\

	Template Name: About

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
					<h2><?php the_field('principles_title'); ?></h2>
				</section>
			<?php endif; ?>

			<div class="banner blue">
				<section class="is-standard-width has-standard-spacing">
					<?php if( have_rows('principles_list_left') ): ?>
						<ul>
							<?php while ( have_rows('principles_list_left') ) : the_row(); ?>
								<li><?php the_sub_field('principle'); ?></li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>

					<?php if( have_rows('principles_list_right') ): ?>
						<ul>
							<?php while ( have_rows('principles_list_right') ) : the_row(); ?>
								<li><?php the_sub_field('principle'); ?></li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</section>
			</div>

			<section id="team" class="team is-standard-width has-standard-spacing">
				<h2>Team</h2>
			</section>
			<section class="team feed grid has-four-column is-large-width has-standard-spacing">
				<?php //QUERY TEAMMEMBERS
					$args = array( 
						'posts_per_page'  => -1, 
						'post_type' 			=> 'teammember',
						'orderby'         => 'wpse_last_word',
    				'order'           => 'ASC'
					);
					$team_query = new WP_Query( $args );
				?>
				<?php if ( $team_query->have_posts() ) : ?>
					<?php while ( $team_query->have_posts() ) : $team_query->the_post(); ?>
						<?php get_template_part( 'template-parts/previews/preview-teammember' ); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</section>

			<section class="is-large-width has-standard-spacing">
				<p class="disclaimer"><?php the_field('disclaimer_text'); ?></p>
			</section>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>