<?php 
/*----------------------------------------------------------------*\

	Template Name: Services

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
					<h2>Who we serve</h2>
				</section>
			<?php endif; ?>

			<?php if( have_rows('persona_card_repeater') ): ?>
				<script>
					var $ = jQuery;

					$(document).ready(function () {
						$(window).scroll(function () {
							drawLine($('#line-fill'),
							document.getElementById('line-path'));
						});

						// init the line length
						drawLine($('#line-fill'),
						document.getElementById('line-path'));

						//draw the line
						function drawLine(container, line) {
							var pathLength = line.getTotalLength(),
							maxScrollTop = $(document).height() - $(window).height(),
							percentDone = $(window).scrollTop() / maxScrollTop,
							length = (percentDone * pathLength) * 2;
							line.style.strokeDasharray = [length, pathLength].join(' ');
						}
					});
				</script>
				<svg id="line-fill-behind" viewBox="0 0 530 1675">
				<path d="M9.9 12.9c1.6 1.8 3.2 3.6 4.7 5.4 73.2 82.2 75.3 271.8 93.7 394s63.8 269.4 159.9 420.8 122.1 253.7 122.1 404.4S373.1 1447.1 520 1663" fill="none" stroke="#e6e8ea" stroke-width="5" stroke-miterlimit="10"/>
					<circle class="line-dots" cx="520.1" cy="1663" r="6"/>
					<circle class="line-dots" cx="390.9" cy="1232" r="6"/>
					<circle class="line-dots" cx="267.8" cy="831.6" r="6"/>
					<circle class="line-dots" cx="107.9" cy="403.5" r="6"/>
					<circle class="line-dots" cx="9.9" cy="12" r="6"/>
				</svg>
				<svg id="line-fill" viewBox="0 0 530 1675">
					<path id="line-path" d="M9.9 12.9c1.6 1.8 3.2 3.6 4.7 5.4 73.2 82.2 75.3 271.8 93.7 394s63.8 269.4 159.9 420.8 122.1 253.7 122.1 404.4S373.1 1447.1 520 1663" fill="none" stroke="#c7995e" stroke-width="5" stroke-miterlimit="10"/>
					<circle class="line-dots" cx="520.1" cy="1663" r="6"/>
					<circle class="line-dots" cx="390.9" cy="1232" r="6"/>
					<circle class="line-dots" cx="267.8" cy="831.6" r="6"/>
					<circle class="line-dots" cx="107.9" cy="403.5" r="6"/>
					<circle class="line-dots" cx="9.9" cy="12" r="6"/>
				</svg>

				<div class="services-slider">

					<?php while ( have_rows('persona_card_repeater') ) : the_row(); ?>
						<div class="services-card">
							<h3><?php the_sub_field('persona_title'); ?></h3>
							<h4><?php the_sub_field('age_range'); ?></h4>
							<h4><?php the_sub_field('asset_range'); ?></h4>
							<p><?php the_sub_field('persona_description'); ?></p>
						</div>
					<?php endwhile; ?>

				</div>

			<?php endif; ?>

			<?php if ( get_field('banner_title') ) : 
				get_template_part('template-parts/sections/banner');
			endif; ?>

			<section class="is-standard-width has-standard-spacing needs">
				<div><?php the_field('needs_content'); ?></div>
				<div><?php the_field('content_left'); ?></div>
				<div><?php the_field('content_right'); ?></div>
			</section>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>