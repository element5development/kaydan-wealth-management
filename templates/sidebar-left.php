<?php 
/*----------------------------------------------------------------*\

	Template Name: Sidebar Left

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block has-left-sidebar">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-standard-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
		</article>
	</main>
	
	<?php get_template_part('template-parts/sidebars/sidebar-left'); ?>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>
	
</div>

<?php get_footer(); ?>