<?php 
/*----------------------------------------------------------------*\

	Template Name: Tools

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>

			<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="main-content-block is-standard-width has-small-spacing">
				<?php the_content(); ?>
			</section>
			<?php endif; ?>

			<section class="split is-standard-width has-small-spacing">
				<div class="resources">
					<h2>Getting Started</h2>
					<?php if( have_rows('resource_repeater') ): ?>
						<?php while ( have_rows('resource_repeater') ) : the_row(); ?>
							<?php $file = get_sub_field('resource'); ?>
							<?php if( !empty($file) ): ?>
								<p>
									<a href="<?php echo $file['url']; ?>" target="_blank">
										<svg>
											<use xlink:href="#file"></use>
										</svg>
										<?php echo $file['title']; ?>
									</a>
								</p>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div class="symbol-lookup">
					<h2>Stock Symbol Lookup</h2>
					<script>
						let search;
						const lastUpdated = new
						Date().toLocaleString();

						var $ = jQuery;
						$(document).ready(function () {
							search = "";
							getStockInfo();
							getStockChart();

							$(".lastUpdated").text(`Last updated: ${lastUpdated}`);

							$("#search button").on("click", function () {
								var searchstring = $(".searchbar");
								search = searchstring.val();
								getStockInfo();
								getStockChart();
								$(".header").removeClass("hidden");
								$("#stockChart").removeClass("hidden");
								$(".lastUpdated").removeClass("hidden");
							});
						})

						function getStockInfo() {
							data = $.ajax({
								type: "GET",
								url: 'https://api.iextrading.com/1.0/stock/' + search + '/quote',
								dataType: "json",
								success: function (data) {
									let name = data.symbol;
									let price = rounder(data.latestPrice, 2);
									let change = data.changePercent;

									$(".header .name").text(name);
									$(".header .price").text(price);
									$(".header .change").text((rounder(change, 4) * 100).toFixed(2) + "%");

									if (change >= 0) {
										$(".header .change").css("color", "#68A19B");
										$(".header .change").prepend("+");
									} else {
										$(".header .change").css("color", "#D85060");
										$(".header .change").prepend("-");
									}
								}
							});
						}

						function getStockChart() {
							new TradingView.MediumWidget({
								"container_id": "tv-medium-widget-5e6f9",
								"symbols": [
									[
										"Google",
										search
									]
								],
								"greyText": "Quotes by",
								"gridLineColor": "#e9e9ea",
								"fontColor": "#0d1e32",
								"underLineColor": "#dbeffb",
								"trendLineColor": "#4bafe9",
								"height": "400px",
								"locale": "en"
							});
						}

						function formatDate() {
							var d = new Date(),
								month = '' + (d.getMonth() + 1),
								day = '' + d.getDate(),
								year = d.getFullYear();

							if (month.length < 2) month = '0' + month;
							if (day.length < 2) day = '0' + day;

							return [year, month, day].join('-');
						}

						function getYesterdayDate() {
							let d = new Date(),
								month = '' + (d.getMonth() + 1),
								day = '' + d.getDate() - 1,
								year = d.getFullYear();

							if (month.length < 2) month = '0' + month;
							if (day.length < 2) day = '0' + day;

							return [year, month, day].join('-');
						}

						function rounder(number, precision) {
							let factor = Math.pow(10, precision);
							return Math.round(number * factor) / factor;
						}

						setInterval(getStockInfo, 10000);
					</script>
					<div class="container">
						<form id="search" onsubmit="return false">
							<label for="stocksearch">Stock Symbol</label>
							<input type="text" id="stocksearch" value="" class="searchbar hidden" placeholder="Enter stock symbol">
							<button>Look up</button>
						</form>
						<div class="header hidden">
							<div class="stock">
								<div class="name"></div>
								<div class="price"></div>
								<div class="change"></div>
							</div>
						</div>
						<div id="stockChart" class="hidden">
							<div id="tv-medium-widget-5e6f9"></div>
						</div>
						<div class="lastUpdated hidden"></div>
					</div>
				</div>
				<!-- <div class="download-app">
					<svg viewBox="0 0 80 80">
						<defs>
							<style>
								.cls-222 {
									fill: none
								}

								.cls-322 {
									fill: #fff
								}
							</style>
						</defs>
						<g id="Layer_1" data-name="Layer 1">
							<rect width="80" height="80" rx="12.86" ry="12.86" fill="#396079" />
							<path class="cls-222" d="M-941-693H499v4527H-941z" />
							<rect class="cls-222" width="80" height="80" rx="12.86" ry="12.86" />
							<path class="cls-322"
								d="M12.93 19c0-.79-.38-1.72-2.28-1.72H8c-.45 0-.6-.09-.6-.45v-.64c0-.29.15-.49.6-.49h17.23c.45 0 .61.15.61.43v.65c0 .36-.16.5-.53.5h-3c-1.52 0-2.12.5-2.12 2.22.01 1.03-.19 12.2-.19 15.7v3.58h1.36a5 5 0 0 0 3.11-1c1.66-1.14 15.68-18 16.36-19s.3-1.72-.68-1.72h-1.1c-.3 0-.37-.14-.37-.5v-.44c0-.35.15-.5.45-.5h12.3c.38 0 .53.15.53.36v.72c0 .36-.23.5-.61.5h-.63a8.21 8.21 0 0 0-4.17 1.36c-2.8 1.94-14.39 14.61-17.64 18.62L49.2 61a6.31 6.31 0 0 0 4.65 1.77h18.39-.15a.46.46 0 0 1 .53.5v.58c0 .35-.15.5-.6.5H37.46a.39.39 0 0 1-.45-.43v-.65c0-.29.15-.43.45-.43h1.14a1.26 1.26 0 0 0 1.21-1.93C39.13 59.47 25.73 42.72 24.74 42a4.87 4.87 0 0 0-3.33-1.07H20v5.16c0 3.58.15 14.1.23 15.1a1.65 1.65 0 0 0 1.74 1.58h3.33c.38 0 .61.14.61.43v.65c0 .28-.08.5-.46.5H7.91c-.38 0-.53-.15-.53-.5v-.65c0-.36.22-.43.53-.43h3c1.36 0 1.9-.57 2-1.29s.02-41.96.02-42.48z" />
							<path class="cls-322"
								d="M65.16 39.08c-.37 0-.49-.28-.6-.56L60.67 27c-.63 1.78-4 11.39-4.16 11.64s-.27.48-.54.48-.41-.14-.51-.43l-4.81-13.8c-.2-.58-.47-.7-.81-.7h-.73a.25.25 0 0 1-.26-.29v-.52c0-.19.1-.28.3-.28h5c.27 0 .3.18.3.26v.54c0 .07 0 .29-.3.29h-.36a.64.64 0 0 0-.48.16.57.57 0 0 0 0 .46l3.37 10.6c.12-.34.33-1 .63-1.87 1-2.86 2.6-7.65 2.66-8.28a1.45 1.45 0 0 0-.3-.71.89.89 0 0 0-.76-.36h-.61c-.26 0-.26-.21-.26-.29v-.52a.26.26 0 0 1 .28-.28h5.48a.25.25 0 0 1 .27.28v.54a.24.24 0 0 1-.27.27h-.87a.51.51 0 0 0-.42.19.57.57 0 0 0-.06.51l3.27 10.53C67.25 30.93 69 25.57 69 24.7c0-.37-.21-.55-.63-.55h-.58c-.28 0-.28-.23-.28-.32v-.49c0-.23.16-.28.3-.28h4.29a.25.25 0 0 1 .28.28v.54c0 .08 0 .27-.28.27h-.51a.92.92 0 0 0-.76.52c-.09.18-1.43 3.82-2.61 7s-2.29 6.23-2.5 6.73c-.14.38-.27.68-.56.68zM64.44 56.59a.27.27 0 0 1-.3-.32v-.49c0-.2.09-.28.3-.28h1.46c.27 0 .44 0 .48-.66v-6.95-5.11c-.41.87-2 4.26-2.66 5.81-.31.67-.52 1.13-.54 1.19s-1.53 3.46-2.21 5l-.39.89c-.17.39-.36.59-.54.59s-.36-.27-.44-.47-3.08-7.58-3.08-7.58c-1.18-2.88-2-4.67-2.3-5.33-.06 1.22-.09 5.19-.09 5.61V54.02c.11 1.25.3 1.45.86 1.45h1.08a.25.25 0 0 1 .27.28v.53c0 .19-.11.28-.32.28h-5.33c-.24 0-.31-.08-.31-.32v-.52a.23.23 0 0 1 .25-.25h1.14c.68 0 .87-.4 1-1.18.25-1.49.32-6.83.32-7.39v-4.17c0-.6 0-.67-.75-.67H51a.25.25 0 0 1-.28-.27v-.56A.25.25 0 0 1 51 41h4.44a.62.62 0 0 1 .68.45l4.67 11.24c.33-.62 1.18-2.56 1.71-3.78.46-1 .72-1.64.78-1.75.22-.43 2.48-5.35 2.54-5.73s.42-.36.62-.36C68.91 41 70 41 70.28 41c.08 0 .25 0 .25.28v.49c0 .09 0 .32-.29.32h-.95c-.5 0-.58.23-.58.76V54.47c0 .86.22 1 .6 1h1.27a.26.26 0 0 1 .28.28v.49c0 .15 0 .32-.32.32z" />
						</g>
					</svg>
					<h3>Download our mobile app</h3>
					<p>For more videos and articles download our app for free by searching for <strong>Kaydan Wealth
							Management</strong> on the App Store or on Google Play.</p>
					<a href="https://itunes.apple.com/us/app/kaydan-wealth-management/id986497290" target="_blank"><img
							src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/download-on-the-app-store-apple.svg"></a>
					<a href="https://play.google.com/store/apps/details?id=com.fmg.KruzanJim" target="_blank"><img
							src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/google-play-download-android-app.svg"></a>
				</div> -->
			</section>

			<!-- <section class="is-standard-width has-small-spacing resources">
				<h2>Getting Started</h2>
				<?php if( have_rows('resource_repeater') ): ?>
					<?php while ( have_rows('resource_repeater') ) : the_row(); ?>
						<?php $file = get_sub_field('resource'); ?>
						<?php if( !empty($file) ): ?>
							<p>
								<a href="<?php echo $file['url']; ?>" target="_blank">
									<svg>
										<use xlink:href="#file"></use>
									</svg>
									<?php echo $file['title']; ?>
								</a>
							</p>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</section> -->

			<section class="is-standard-width has-small-spacing videos">

				<?php if( have_rows('video_repeater') ): ?>
				<?php while ( have_rows('video_repeater') ) : the_row(); ?>

				<div>

					<h2 id="<?php the_sub_field('anchor'); ?>"><?php the_sub_field('video_title'); ?></h2>

					<button href="<?php the_sub_field('video_link'); ?>" data-featherlight="iframe"
						data-featherlight-iframe-width="640" data-featherlight-iframe-height="480"
						data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media"
						data-featherlight-iframe-allowfullscreen="true">
						<img src="<?php the_sub_field('video_thumbnail'); ?>" alt="Video Thumbnail">
						<span>
							<svg>
								<use xlink:href="#play" />
							</svg>
						</span>
						<div class="overlay"></div>
					</button>

					<?php $link = get_sub_field('video_cta'); ?>

					<?php if( $link ): ?>
					<a class="button is-primary" href="<?php echo $link['url']; ?>"
						target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>

				</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</section>

			<section class="is-standard-width has-standard-spacing cards">

				<?php if( have_rows('card_repeater') ): ?>
				<?php while ( have_rows('card_repeater') ) : the_row(); ?>

				<div>

					<?php $image = get_sub_field('card_icon'); ?>

					<?php if( !empty($image) ): ?>
					<img id="<?php the_sub_field('anchor'); ?>" src="<?php echo $image['url']; ?>"
						alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>

					<h2><?php the_sub_field('card_title'); ?></h2>

					<?php the_sub_field('card_content'); ?>

					<p class="disclaimer"><?php the_sub_field('card_disclaimer'); ?></p>

				</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</section>

			<section class="is-standard-width has-small-spacing seminars">

				<h2>Benefits of a Second Opinion</h2>

				<?php if( have_rows('seminar_repeater') ): ?>
				<?php while ( have_rows('seminar_repeater') ) : the_row(); ?>

				<div>

					<?php $image = get_sub_field('seminar_icon'); ?>

					<?php if( !empty($image) ): ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>

					<h3><?php the_sub_field('seminar_title'); ?></h3>

					<?php $link = get_sub_field('seminar_cta'); ?>

					<?php if( $link ): ?>
					<a class="button is-primary" href="<?php echo $link['url']; ?>"
						target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>

					<?php if( have_rows('seminar_resources') ): ?>
					<?php while ( have_rows('seminar_resources') ) : the_row(); ?>

					<?php $file = get_sub_field('seminar_resource'); ?>

					<?php if( !empty($file) ): ?>
					<p>
						<a href="<?php echo $file['url']; ?>" target="_blank">
							<svg>
								<use xlink:href="#file"></use>
							</svg>
							<?php echo $file['title']; ?>
						</a>
					</p>
					<?php endif; ?>

					<?php endwhile; ?>
					<?php endif; ?>

				</div>

				<?php endwhile; ?>
				<?php endif; ?>

			</section>

			<section class="is-standard-width has-small-spacing section-links">

				<h2>Helpful Links</h2>

				<?php if( have_rows('link_repeater') ): ?>
				<?php while ( have_rows('link_repeater') ) : the_row(); ?>

				<?php $link = get_sub_field('tool_link'); ?>

				<?php if( $link ): ?>
				<p>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
						<svg>
							<use xlink:href="#link"></use>
						</svg>
						<?php echo $link['title']; ?>
					</a>
				</p>
				<?php endif; ?>

				<?php endwhile; ?>
				<?php endif; ?>

			</section>

			<section class="is-large-width has-standard-spacing">
				<p class="disclaimer"><?php the_field('disclaimer_text'); ?></p>
			</section>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>