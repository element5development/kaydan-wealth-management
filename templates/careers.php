<?php 
/*----------------------------------------------------------------*\

	Template Name: Careers

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">

	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>

	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<section class="main-content-block is-standard-width has-small-spacing">
					<?php the_content(); ?>
				</section>
			<?php endif; ?>
				
			<?php if( have_rows('card_repeater') ): ?>

				<section class="is-standard-width has-small-spacing cards">
					<h2>We are…</h2>

					<?php while ( have_rows('card_repeater') ) : the_row(); ?>

						<div class="card">

							<?php $image = get_sub_field('card_icon'); ?>

							<?php if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>

							<h3><?php the_sub_field('card_title'); ?></h3>

							<?php the_sub_field('card_content'); ?>

							<p class="disclaimer"><?php the_sub_field('card_disclaimer'); ?></p>

						</div>

					<?php endwhile; ?>

					<h2>We are your team.</h2>
				</section>

			<?php endif; ?>

			<section class="is-standard-width has-standard-spacing jobs">
				<h2>Current Openings</h2>

				<?php //QUERY JOBS
					$args = array( 
						'posts_per_page'  => -1, 
						'post_type' 			=> 'job',
					);
					$jobs_query = new WP_Query( $args );
				?>
				<?php if ( $jobs_query->have_posts() ) : ?>
					<?php while ( $jobs_query->have_posts() ) : $jobs_query->the_post(); ?>
						<?php get_template_part( 'template-parts/previews/preview-job' ); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>

				<div class="looking-for">
					<h3>Want to apply or don’t see what you’re looking for?</h3>
					<p>We’re always on the lookout for talented professionals to join our team.</p>
					<p>Drop us a line at <a href="mailto:careers@kaydanwealth.com">careers@kaydanwealth.com</a> and let us know how you stand out from the crowd.</>
				</div>
			</section>

		</article>
	</main>

	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>

</div>

<?php get_footer(); ?>