<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>
<footer class="page-footer">
	<section class="grid is-large-width">
		<div class="block">
			<?php dynamic_sidebar('left-footer'); ?>
		</div>
		<div class="block buttons">
			<?php dynamic_sidebar('center-footer'); ?>
		</div>
		<div class="block">
			<?php dynamic_sidebar('right-footer'); ?>
		</div>
		<div class="block social">
			<a href="<?php the_field('facebook', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook" />
				</svg>
			</a>
			<a href="<?php the_field('linkedin', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
			<a href="<?php the_field('twitter', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#twitter" />
				</svg>
			</a>
			<a href="<?php the_field('youtube', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#youtube" />
				</svg>
			</a>
			<!-- <a href="<?php the_field('apple', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#apple" />
				</svg>
			</a>
			<a href="<?php the_field('android', 'option'); ?>" target="_blank">
				<svg>
					<use xlink:href="#android" />
				</svg>
			</a> -->
		</div>
		<div class="block wide">
			<?php dynamic_sidebar('bottom-footer'); ?>
		</div>
		<div id="element5-credit" data-emergence="hidden">
			<a target="_blank" href="https://element5digital.com">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
	</section>
</footer>