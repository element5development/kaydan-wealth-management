<?php 
/*----------------------------------------------------------------*\

	SIMPLE FOOTER
	Used commonly on landing pages

\*----------------------------------------------------------------*/
?>

<footer class="page-footer">
	<div class="copyright">
		<section class="grid is-standard-width has-small-spacing">
			<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		</section>
	</div>
</footer>