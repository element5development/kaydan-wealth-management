<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<h1 class="has-subheader is-padded"><?php the_title(); ?></h1>
	<p class="subheader is-padded"><?php echo get_excerpt(150); ?></p>
	<div class="buttons is-padded">
		<div class="button is-ghost">Read This Post</div>
	</div>
	<a href="<?php the_permalink(); ?>"></a>
</article>