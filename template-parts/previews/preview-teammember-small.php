<?php 
/*----------------------------------------------------------------*\

	SMALL PREVIEW ELEMENT FOR TEAM MEMBER POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="featured-image has-overlay">
		<?php if ( get_field('headshot') ) : ?>
			<?php $image = get_field('headshot'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php else : ?>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-bio-image.jpg" alt="default headshot" />
		<?php endif; ?>
		<?php if ( get_field('baby_photo') ) : ?>
			<?php $babyimage = get_field('baby_photo'); ?>
			<img src="<?php echo $babyimage['sizes']['small']; ?>" alt="<?php echo $babyimage['alt']; ?>" />
		<?php else : ?>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-bio-image.jpg" alt="default headshot" />
		<?php endif; ?>
	</div>
	<div class="member-info">
		<h1 class="has-subheader is-padded"><?php the_title(); ?></h1>
		<p class="subheader is-padded"><?php the_field('titles'); ?></p>
		<div class="links is-padded">
			<?php if ( get_field('email') ) : ?>
				<a href="mailto:<?php the_field('email'); ?>" target="_blank">
					<svg width="22" height="16" viewBox="0 0 22 16">
						<path d="M20 0H1.3333C.9723 0 .6597.132.3958.3958.132.6598 0 .9722 0 1.3333v13.3334c0 .361.132.6736.3958.9375.264.2639.5764.3958.9375.3958H20c.3611 0 .6736-.132.9375-.3958.2639-.264.3958-.5764.3958-.9375V1.3333c0-.361-.132-.6736-.3958-.9375C20.6736.132 20.3611 0 20 0zM7.5833 9.375l3.0834 2.0833L13.75 9.375l5.2917 5.2917h-16.75L7.5833 9.375zm-6.25 4.3333V5.125l5.125 3.5-5.125 5.0833zm13.5417-5.125L20 5.0417v8.6666l-5.125-5.125zM20 1.3333V3.5l-.0417-.0417-9.2916 6.4167L1.3333 3.5V1.3333H20z" fill-rule="evenodd"/>
					</svg>
				</a>
			<?php endif; ?>
			<?php if ( get_field('linkedin') ) : ?>
				<a href="<?php the_field('linkedin'); ?>" target="_blank">
					<svg width="22" height="16" viewBox="0 0 24 24">
						<path d="M4.98 3.5C4.98 4.881 3.87 6 2.5 6S.02 4.881.02 3.5C.02 2.12 1.13 1 2.5 1s2.48 1.12 2.48 2.5zM5 8H0v16h5V8zm7.982 0H8.014v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0V24H24V13.869c0-7.88-8.922-7.593-11.018-3.714V8z"/>
					</svg>
				</a>
			<?php endif; ?>
		</div>
	</div>
	<a href="<?php the_permalink(); ?>"></a>
</article>