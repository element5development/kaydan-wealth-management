<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR EVENTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post event-preview">
	<div class="categories">
		<?php 
			$categories = get_the_terms( $post->ID , 'type' );
			if ( ! empty( $categories ) ) {
				foreach( $categories as $category ) {
					$category_link = get_term_link( $category, 'type' );
					echo '<a href="' . $category_link . '">' . $category->name . '</a> ';
				}
			}
		?>
	</div>
	<?php if ( get_field('event_date') ) : ?>
		<p class="has-date"><?php the_field('event_date'); ?></p>
	<?php endif; ?>
	<h1 class="has-subheader"><?php the_title(); ?></h1>
	<?php if ( get_field('event_description') ) : ?>
		<p class="subheader"><?php the_field('event_description'); ?></p>
	<?php endif; ?>
	<?php if ( get_field('start_time') ) : ?>
		<h2><?php the_field('start_time'); ?> - <?php the_field('end_time'); ?></h2>
	<?php endif; ?>
	<?php if ( get_field('event_location') ) : ?>
		<h3><?php the_field('event_location'); ?><br><?php the_field('address'); ?><br><?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip_code'); ?></h3>
	<?php endif; ?>
	<div class="buttons">
		<?php
		$link = get_field('event_link');
		if( $link ): ?>
			<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<div title="Add to Calendar" class="addeventatc">
				Add to my calendar
				<?php 
					$startdate = get_field('event_date', false, false);
					$startdate = new DateTime($startdate);
				?>
				<span class="start"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('start_time'); ?></span>
				<span class="end"><?php echo $startdate->format('m/d/Y'); ?> <?php the_field('end_time'); ?></span>
				<span class="timezone">America/Detroit</span>
				<span class="title"><?php the_title(); ?></span>
				<span class="description"><?php the_field('event_description'); ?></span>
				<span class="location"><?php the_field('event_location'); ?></span>
			</div>
		<?php endif; ?>
	</div>
</article>