<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<?php $image = get_field('featured_img'); ?>
	<?php if ( get_field('featured_img') ) : ?>
		<div class="featured-image has-overlay">
			<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	<?php endif; ?>
	<div class="categories">
		<?php 
			$categories = get_the_category();
			$separator = ' ';
			$output = '';
			if ( ! empty( $categories ) ) {
				foreach( $categories as $category ) {
					$output .= '<a class="chip" href="'.get_category_link( $category->term_id ).'">' . esc_html( $category->name ) . '</a>' . $separator;
				}
				echo trim( $output, $separator );
			}
		?>
	</div>
	<p class="has-date is-padded"><?php echo get_the_date('F j, Y'); ?></p>
	<h1 class="has-subheader is-padded"><?php the_title(); ?></h1>
	<h3 class="has-author is-padded">by: <?php the_author(); ?></h3>
	<p class="subheader is-padded"><?php echo get_excerpt(150); ?></p>
	<div class="buttons is-padded">
		<div class="button is-ghost">Read This Post</div>
	</div>
	<a href="<?php the_permalink(); ?>"></a>
</article>
