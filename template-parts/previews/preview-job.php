<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR JOB POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<div class="job-info">
		<p>Posted <?php the_date('F Y'); ?></p>
		<h1><?php the_title(); ?></h1>

		<?php $link = get_field('job_link'); ?>

		<?php if( !empty($link) ): ?>
			<a class="button is-primary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>

	</div>
	<?php 
	if( $link ): 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
	?>
	<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"></a>
	<?php endif; ?>
</article>