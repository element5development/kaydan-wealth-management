<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED POSTS BY CATEGORY

\*----------------------------------------------------------------*/
?>

<?php
	$categories = get_the_category();
	$category_id = $categories[0]->cat_ID;
?>

<p>Do you need more information on this topic? <button class="helpful-back">Request more information</button></p>
<h2 class="related">Other posts you might be interested in</h2>
<section class="grid is-large-width has-three-column has-standard-spacing">
	<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'order' => 'ASC',
			'post__not_in' => array( $post->ID ),
			'cat' => $category_id,
		);
		$loop = new WP_Query( $args );
		?>
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
			<?php get_template_part( 'template-parts/previews/preview-blog' ); ?>
		<?php endwhile;?>
		<?php wp_reset_query(); ?>
</section>