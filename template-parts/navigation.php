<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>
<div class="utility-block">
	<?php
		$args = array(
			'post_type'				=> 'event',
			'posts_per_page'	=> 1,
			'meta_key'				=> 'event_date',
			'orderby'					=> 'meta_value',
			'order'           => 'ASC',
			'meta_query' 			=> array(
				array(
					'key' 			=> 'featured_event',
					'compare' 	=> '=',
					'value' 		=> '1'
				)
			)
		);
		$events = new WP_Query( $args );
	?>
	<?php if ( $events->have_posts() ) { ?>
		<div class="event-area">
			<h3>Featured Event:</h3>
			<?php while ( $events->have_posts() ) : $events->the_post(); ?>
				<p><?php the_field('event_date'); ?>: <?php the_title(); ?> at <?php the_field('start_time'); ?>… <a href="<?php echo get_site_url(); ?>/events/">learn more</a></p>
			<?php endwhile; ?>
		</div>
	<?php } wp_reset_postdata(); ?>

	<div class="phone-block">
		<h3>Call Us</h3>
		<a href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('primary_phone_number', 'option')); ?>"><?php the_field('primary_phone_number', 'option'); ?></a>
	</div>
</div>
<div class="navigation-block">
	<a href="<?php echo get_home_url(); ?>">
		<svg>
			<use xlink:href="#logo" />
		</svg>
	</a>
	<div class="hamburger-menu"></div>
	<nav>
		<button id="search-button">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
		<div id="search-form">
			<?php get_search_form(); ?>
			<button id="search-close">
				<span>Cancel</span>
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
		</div>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
		<a class="button is-ghost is-primary" href="<?php echo get_site_url(); ?>/book-an-appointment/">Book An Appointment</a>
		<h3>Call Us <a href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('primary_phone_number', 'option')); ?>"><?php the_field('primary_phone_number', 'option'); ?></a></h3>
		<p>Auburn Hills <a href="tel:+12486252993">(248) 625-2993</a></p>
		<p>Fenton <a href="tel:+18105931624">(810) 593-1624</a></p>
		<div class="close"></div>
	</nav>
	<a id="login-button" href="https://investoraccess.rjf.com/" target="_blank">Client Access</a>
</div>