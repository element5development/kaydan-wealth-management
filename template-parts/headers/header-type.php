<?php 
/*----------------------------------------------------------------*\

	HEADER FOR TYPES

\*----------------------------------------------------------------*/
?>
<?php 
	$taxonomy = get_queried_object();
?>
<header class="page-title has-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-header.jpg');">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<h1><?php echo  $taxonomy->name; ?></h1>
		</div>

	</section>
</header>