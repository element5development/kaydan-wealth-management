<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<header class="page-title">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<h1>
				<?php 
					if ( get_field('page_title') ) :
						the_field('page_title');
					else :
						the_title();
					endif;
				?>
			</h1>
			<?php if ( get_field('title_description') ) : ?>
				<h3>
				<?php the_field('title_description'); ?>
				</h3>
			<?php endif; ?>
		</div>

		<?php if ( have_rows('title_buttons') ) : $i = 1; ?>
			<div class="buttons">
				<?php while( have_rows('title_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<?php if ( $i == 3 ) :
						$class = 'is-secondary';
					elseif ( $i == 2 ) :
						$class = 'is-tertiary';
					else :
						$class = 'is-primary';
					endif; ?>
					<a class="button <?php echo $class; ?>" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php $i++; endwhile; ?>
			</div>
		<?php endif; ?>

	</section>
</header>