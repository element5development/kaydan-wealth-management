<?php 
/*----------------------------------------------------------------*\

	HEADER WITH FEATURED POST

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-header.jpg');">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<?php //QUERY LATEST
				$args = array( 
					'posts_per_page'  => 1,
					'order'           => 'DESC'
				);
				$featured_query = new WP_Query( $args );
			?>
			<?php if ( $featured_query->have_posts() ) : ?>
				<?php while ( $featured_query->have_posts() ) : $featured_query->the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<h3><?php echo get_excerpt(50); ?></h3>
					<a class="button" href="<?php the_permalink(); ?>">Read this Post</a>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>

	</section>

</header>