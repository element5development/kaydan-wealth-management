<?php 
/*----------------------------------------------------------------*\

	TEAMMEMBER HEADER

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-team-header.jpg');">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<?php if ( get_field('headshot') ) : ?>
				<?php $image = get_field('headshot'); ?>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php else : ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-bio-image.jpg" alt="default headshot" />
			<?php endif; ?>
			<h1>
				<?php 
					if ( get_field('page_title') ) :
						the_field('page_title');
					else :
						the_title();
					endif;
				?>
			</h1>
			<?php if ( get_field('titles') ) : ?>
				<h3><?php the_field('titles'); ?></h3>
			<?php endif; ?>
			<?php if ( get_field('email') ) : ?>
				<?php
					$s = get_the_title();
					$arr1 = explode(' ',trim($s));
				?>
				<a class="button is-primary" href="mailto:<?php the_field('email'); ?>" target="_blank">Email <?php echo $arr1[0]."\n"; ?></a>
			<?php endif; ?>

			<?php $link = get_field('book_appointment'); ?>

			<?php if( !empty($link) ): ?>
				<a class="button is-ghost is-tertiary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>

	</section>

</header>