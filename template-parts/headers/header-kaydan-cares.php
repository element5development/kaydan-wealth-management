<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND FOR KAYDAN CARES

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image" style="background-image: linear-gradient(rgba(44,81,124,0.65), rgba(44,81,124,0.65)), url('<?php the_field('title_bg_img') ?>');">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<svg>
				<use xlink:href="#kaydan-cares-logo"></use>
			</svg>
			<p>
				<?php 
					if ( get_field('page_title') ) :
						the_field('page_title');
					else :
						the_title();
					endif;
				?>
			</p>
		</div>

	</section>


</header>