<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<?php $image = get_field('featured_img'); ?>
<header class="page-title has-image" style="background-image: url('<?php echo $image['sizes']['xlarge']; ?>');">
	<section class="block is-standard-width has-standard-spacing">

		<div class="blocks">
			<div class="categories">
				<?php 
					$categories = get_the_category();
					$separator = ' ';
					$output = '';
					if ( ! empty( $categories ) ) {
						foreach( $categories as $category ) {
								$output .= '<a class="chip is-ghost is-primary" href="'.get_category_link( $category->term_id ).'">' . esc_html( $category->name ) . '</a>' . $separator;
						}
						echo trim( $output, $separator );
					}
				?>
			</div>
			<h1>
				<?php 
					if ( get_field('post_title') ) :
						the_field('post_title');
					else :
						the_title();
					endif;
				?>
			</h1>
		</div>

	</section>

</header>