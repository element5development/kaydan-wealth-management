<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types and search results

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');

	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
?>
<?php 
	$taxonomy = get_queried_object();
?>

<header class="page-title has-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-header.jpg');">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<?php if ( get_field( $posttype . '_page_title', 'option') ) : ?>
				<h1><?php the_field( $posttype . '_page_title', 'option'); ?></h1>
			<?php elseif ( get_queried_object() ) : ?>
				<h1><?php echo  $taxonomy->name; ?></h1>
			<?php else : ?>
				<h1><?php the_title(); ?></h1>
			<?php endif; ?>
			<?php if ( get_field( $posttype . '_page_sub_title', 'option') ) : ?>
				<h3 class="subheader"><?php the_field( $posttype . '_page_sub_title', 'option'); ?></h3>
			<?php endif; ?>
		</div>

	</section>
</header>