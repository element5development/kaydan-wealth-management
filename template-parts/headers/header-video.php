<?php 
/*----------------------------------------------------------------*\

	HEADER WITH VIDEO BACKGROUND

\*----------------------------------------------------------------*/
?>

<header class="page-title has-video" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
	<section class="block is-large-width has-standard-spacing">

		<div class="blocks">
			<h1>
				<?php 
					if ( get_field('page_title') ) :
						the_field('page_title');
					else :
						the_title();
					endif;
				?>
			</h1>
			<?php if ( get_field('title_description') ) : ?>
				<h3>
				<?php the_field('title_description'); ?>
				</h3>
			<?php endif; ?>
			<?php if ( have_rows('title_buttons') ) : $i = 1; ?>
				<div class="buttons">
					<?php while( have_rows('title_buttons') ) : the_row(); ?>
						<?php $button = get_sub_field('button'); ?>
						<?php if ( $i == 3 ) :
							$class = 'is-secondary';
						elseif ( $i == 2 ) :
							$class = 'is-tertiary';
						else :
							$class = 'is-primary';
						endif; ?>
						<a class="button <?php echo $class; ?>" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
							<?php echo $button['title']; ?>
						</a>
					<?php $i++; endwhile; ?>
				</div>
			<?php endif; ?>
		</div>

	</section>

	<button href="<?php the_field('title_bg_vid'); ?>?rel=0&showinfo=0" data-featherlight="iframe" data-featherlight-iframe-width="640" data-featherlight-iframe-height="480" data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media" data-featherlight-iframe-allowfullscreen="true">
		<svg>
			<use xlink:href="#play" />
		</svg>
	</button>

</header>