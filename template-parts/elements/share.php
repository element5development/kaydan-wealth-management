<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED POSTS BY CATEGORY

\*----------------------------------------------------------------*/
?>

<?php 
	$post_url = get_permalink();
	$post_content = get_the_excerpt();
	$featured_img = get_home_url() . get_the_post_thumbnail_url();
	$post_title = get_the_title();
?>

<section id="social-share-fixed" class="social-share">
	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post_url; ?>" class="button is-icon is-ghost is-borderless" title="Share on Facebook">
		<svg>
			<use xlink:href="#facebook-2" />
		</svg>
	</a>
	<a target="_blank" href="https://twitter.com/home?status=<?php echo $post_content; echo $post_url; ?>" class="button is-icon is-ghost is-borderless" title="Share on Twitter">
		<svg>
			<use xlink:href="#twitter-2" />
		</svg>
	</a>
	<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" class="button is-icon is-ghost is-borderless" title="Share on Linkedin">
		<svg>
			<use xlink:href="#linkedin-2" />
		</svg>
	</a>
</section>